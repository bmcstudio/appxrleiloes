(function(){

	'use strict';

	/* jshint validthis: true */

	angular.module('AppXRleiloes.controllers')
		.controller('cadastroCtrl', cadastroCtrl);

	cadastroCtrl.$inject = ['$api', '$ionicNavBarDelegate'];

	function cadastroCtrl($api, $ionicNavBarDelegate) {

		var vm = this;
		vm.formcadastro = {};

		vm.goBack       = goBack;
		vm.buyHorse		= buyHorse;
		vm.sendCadastre = sendCadastre;

		///////////////////////


		if (window.StatusBar) StatusBar.styleLightContent();

		function goBack() {
			$ionicNavBarDelegate.back();
		}

		function buyHorse() {
			if (vm.formcadastro.refcomercial.comproucavalo == 0)
				document.getElementById('wherebuy').focus();
			else
				vm.formcadastro.refcomercial.onde = '';
		}

		function sendCadastre() {

			var date = new Date(vm.formcadastro.info.nascimento).toLocaleDateString('pt-BR'),
				year = date.substr(6,4),
				currentYear = new Date().getFullYear(),
				isOlder = currentYear - year;

			if (isOlder < 18) {
				navigator.notification.alert(
					'Você precisa ter mais que 18 anos para se cadastrar.',  // message
					null,         	// callback
					'Atenção',    	// title
					'OK'          	// buttonName
				);
				vm.formcadastro.info.nascimento = '';
				return;
			}

			$api.sendCadastre(vm.formcadastro).then(function(response){
				// console.log(response);
				navigator.notification.alert(
					'Você foi cadastrado com sucesso.',  // message
					null,         	// callback
					'Obrigado',    	// title
					'OK'          	// buttonName
				);

				goBack();

			}, function(error){
				// console.log(error);
				navigator.notification.alert(
					error.data,  // message
					null,        // callback
					'Atenção',   // title
					'OK'         // buttonName
				);
			});
		}

	}

})();
