(function() {

	'use strict';

	/* jshint validthis: true */

	angular.module('AppXRleiloes.controllers')
		.controller('loginCtrl', loginCtrl);

	loginCtrl.$inject = ['$api', '$ionicLoading', '$state', 'localStorageService'];

	function loginCtrl($api, $ionicLoading, $state, localStorageService) {

		var vm = this;

		vm.loginData        = {};
		vm.doLogin          = doLogin;
		vm.openExternalLink = openExternalLink;

		///////////////////////


		function doLogin() {
			$ionicLoading.show({
				template: ' <div> \
								<i class="icon ion-loading-c"></i> \
								<p>Verificando</p> \
							</div>'
			});
			$api.login(vm.loginData).then(doneLogin, failLogin);
		}

		function doneLogin(response) {
			$ionicLoading.hide();
			localStorageService.set(0, response.data);
			$state.go('app.home');
		}

		function failLogin(response) {
			$ionicLoading.hide();
			navigator.notification.alert(
				response.data,  // message
				null,         	// callback
				'Atenção',    	// title
				'OK'          	// buttonName
			);
		}

		function openExternalLink() {
			window.open('http://www.xrleiloes.com.br/leilao/registration.html', '_system', 'location=no');
		}
	}

})();