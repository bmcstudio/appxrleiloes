(function(){

	'use strict';

	/* jshint validthis: true */

	angular.module('AppXRleiloes.controllers')
		.controller('lotesCtrl', lotesCtrl);

	lotesCtrl.$inject = ['requestLotes', 'localStorageService', '$stateParams', '$api', '$scope', '$interval', '$ionicModal', '$ionicNavBarDelegate', '$ionicLoading'];

	function lotesCtrl(requestLotes, localStorageService, $stateParams, $api, $scope, $interval, $ionicModal, $ionicNavBarDelegate, $ionicLoading) {

		var vm = this;

		vm.lotes = requestLotes.data;
		vm.regex = /[\\?\\&]v=([^\\?\\&]+)/;
		vm.watch_info = {};
		vm.valoresLance = [
			{ moeda: "R$ 10,00", valor: 10 },
			{ moeda: "R$ 20,00", valor: 20 },
			{ moeda: "R$ 30,00", valor: 30 },
			{ moeda: "R$ 40,00", valor: 40 },
			{ moeda: "R$ 50,00", valor: 50 },
		    { moeda: "R$ 100,00", valor: 100 },
		    { moeda: "R$ 150,00", valor: 150 },
		    { moeda: "R$ 200,00", valor: 200 },
		    { moeda: "R$ 250,00", valor: 250 },
		    { moeda: "R$ 300,00", valor: 300 },
		    { moeda: "R$ 350,00", valor: 350 },
		    { moeda: "R$ 400,00", valor: 400 },
		    { moeda: "R$ 450,00", valor: 450 },
		    { moeda: "R$ 500,00", valor: 500 },
		    { moeda: "R$ 550,00", valor: 550 },
		    { moeda: "R$ 600,00", valor: 600 },
		    { moeda: "R$ 650,00", valor: 650 },
		    { moeda: "R$ 700,00", valor: 700 },
		    { moeda: "R$ 750,00", valor: 750 },
		    { moeda: "R$ 800,00", valor: 800 },
		    { moeda: "R$ 850,00", valor: 850 },
		    { moeda: "R$ 900,00", valor: 900 },
		    { moeda: "R$ 950,00", valor: 950 },
		    { moeda: "R$ 1000,00", valor: 1000 }
		];

		vm.darLance      = darLance;
		vm.openInfo      = openInfo;
		vm.closeModal    = closeModal;
		vm.goBack        = goBack;
		vm.openExternalLink = openExternalLink;

		// console.log(vm.lotes[0]);

		///////////////////////

		$ionicLoading.hide();

		// info modal
		$ionicModal.fromTemplateUrl('template/infoModal.html', {
			scope: $scope
		}).then(function(modal) {
			vm.modal = modal;
		});

		var userID   = localStorageService.get(0).userID,
			leilaoID = $stateParams.lotesID,
			timezone = new Date().getTimezoneOffset() * 60;

		// enter frame
        function updateLotes() {
			$api.updateLotes(userID, leilaoID).then(function(response){

				vm.watch_info = response.data.leilao;
				// console.log(vm.watch_info);

				if (response.data.leilao.status == 3) return;

				var seconds  = Math.floor(Date.now() / 1000);
				var countdown = response.data.leilao.tempo - seconds + timezone;

				// $scope.$broadcast('timer-add-cd-seconds', countdown);
				$scope.$broadcast('timer-set-countdown', countdown);

				// console.log(response.data.leilao.tempo);
				// console.log(seconds);
				// console.log(countdown);

				updateLotes();
			});
        }
        updateLotes();

		function darLance(lote, valorLance, status) {

			if (status == 1) return;

			$api.darLance(userID, lote.auctionID, lote.productID, valorLance);
		}

		function openInfo(data) {
			$scope.detail = data;
			vm.modal.show();
		}

		function closeModal() {
			vm.modal.hide();
		}

		function goBack() {
			$ionicNavBarDelegate.back();
		}

		function openExternalLink(link) {
			window.open(link, '_system', 'location=no');
		}

	}

})();