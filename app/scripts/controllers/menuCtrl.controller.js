(function(){

	'use strict';

	/* jshint validthis: true */

	angular.module('AppXRleiloes.controllers')
		.controller('menuCtrl', menuCtrl);

	menuCtrl.$inject = ['$state', 'localStorageService'];

	function menuCtrl($state, localStorageService) {

		var vm = this;

		vm.logout = logout;

		///////////////////////


		function logout() {
			localStorageService.clearAll();
			$state.go('login');
		}

	}

})();