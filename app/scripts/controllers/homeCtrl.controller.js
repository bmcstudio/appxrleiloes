(function(){

	'use strict';

	/* jshint validthis: true */

	angular.module('AppXRleiloes.controllers')
		.controller('homeCtrl', homeCtrl);

	homeCtrl.$inject = ['requestLeiloes', '$rootScope', '$ionicLoading'];

	function homeCtrl(requestLeiloes, $rootScope, $ionicLoading) {

		var vm = this;

		vm.leiloes = requestLeiloes.data;
		vm.openRegulamento = openRegulamento;

		///////////////////////


		$ionicLoading.hide();

		$rootScope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams){

			if (toState.name == 'login') return;

	        $ionicLoading.show({
				template: ' <div> \
								<i class="icon ion-loading-c"></i> \
								<p>Carregando lotes</p> \
							</div>'
			});
	    });

	    // intercept when there is no 'lotes'
		$rootScope.$on('$stateChangeError', function(next, current) {
			navigator.notification.alert(
				'Nenhum lote cadastrado.',  // message
				null,         	// callback
				'Atenção',    	// title
				'OK'          	// buttonName
			);

			$ionicLoading.hide();
		});

	    function openRegulamento(url) {
	    	if (url == '')
	    		window.open('http://www.xrleiloes.com.br/leilao/regulamento/Regulamento_padrao.pdf', '_system', 'location=no');
	    	else
	    		window.open('http://www.xrleiloes.com.br/leilao/regulamento/' + url, '_system', 'location=no');
	    }

	}

})();