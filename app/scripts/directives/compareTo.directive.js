(function() {

	'use strict';

	/* jshint validthis: true */

	angular.module('AppXRleiloes.directives')
		.directive('compareTo', compareTo);

	function compareTo($q, $http) {

		var directive = {
			require: "ngModel",
	        scope: {
	            otherModelValue: "=compareTo"
	        },
	        link: link
	    };

        return directive;

        /////////////////////////////////

        function link(scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
	}

})();