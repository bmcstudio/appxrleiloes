(function() {

	'use strict';

	/* jshint validthis: true */

	angular.module('AppXRleiloes.services', [])
		.factory('$api', api);

	api.$inject = ['$q', '$http'];

	function api($q, $http) {

		var urlBase = 'http://www.xrleiloes.com.br/leilao/app/',
			api = {
				login        : login,
				getLeiloes   : getLeiloes,
				getLotes     : getLotes,
				updateLotes  : updateLotes,
				darLance     : darLance,
				sendCadastre : sendCadastre
			};

		return api;

		///////////////////////////////////////


		function login(loginData) {
			return $http.get( urlBase + 'verifyPass.php', { params: loginData} );
		}

		function getLeiloes() {
			return $http.get( urlBase + 'onlineAuctions.php' );
		}

		function getLotes(leilaoID) {
			return $http.get( urlBase + 'lotes.php', { params: { id: leilaoID }} );
		}

		function updateLotes(userID, leilaoID) {
			return $http.get( urlBase + 'update_information.php', { params: {uid: userID, cid: leilaoID}} );
		}

		function darLance(userID, loteID, cavaloID, valorLance) {
			return $http({ headers: {'Content-Type': 'application/x-www-form-urlencoded'}, url: urlBase + 'giveBid.php', method: "POST", data: { uid: userID, aid: loteID, pid: cavaloID, value: valorLance} });
		}

		function sendCadastre(params) {
			return $http({ headers: {'Content-Type': 'application/x-www-form-urlencoded'}, url: urlBase + 'cadastro.php', method: "POST", data: params });
		}
	}

})();