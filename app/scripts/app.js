(function() {

    'use strict';

    // app: XRleilões
    // developer by: Douglas de Oliveira

    angular.module('AppXRleiloes.filters', []);
    angular.module('AppXRleiloes.services', []);
    angular.module('AppXRleiloes.directives', []);
    angular.module('AppXRleiloes.controllers', []);

    angular.module('AppXRleiloes', ['ionic', 'config', 'LocalStorageModule', 'timer', 'ui.utils.masks', 'AppXRleiloes.httpInterceptor', 'AppXRleiloes.filters', 'AppXRleiloes.services', 'AppXRleiloes.directives', 'AppXRleiloes.controllers'])

    .run(function($ionicPlatform, $ionicTabsConfig, $ionicLoading, $state, localStorageService) {
        $ionicPlatform.ready(function() {
            if (typeof(navigator.notification) == "undefined") {
              navigator.notification = window;
            }
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                StatusBar.styleLightContent();
            }
            // Default to ios tab-bar style on android too
            $ionicTabsConfig.type = '';

            // checks are logged in
            if (localStorageService.get(0) != null){
                $ionicLoading.show({
                    template: ' <div> \
                                    <i class="icon ion-loading-c"></i> \
                                    <p>Entrando</p> \
                                </div>'
                });

                $state.go('app.home');
            }
        });
    })

    .config(function($stateProvider, $urlRouterProvider, $httpProvider) {

        $stateProvider

        .state('login', {
            url: '/login',
            templateUrl: 'templates/login.html',
            controller: 'loginCtrl as vm'
        })
        .state('app', {
            url: '/app',
            abstract: true,
            templateUrl: 'templates/menu.html',
            controller: 'menuCtrl as vm'
        })
        .state('app.cadastro', {
            url: '/cadastro',
            views: {
                'menuContent': {
                    templateUrl: 'templates/cadastro.html',
                    controller: 'cadastroCtrl as vm'
                }
            }
        })
        .state('app.home', {
            url: '/home',
            views: {
                'menuContent': {
                    templateUrl: 'templates/home.html',
                    controller: 'homeCtrl as vm',
                    resolve: {
                        requestLeiloes: function($api) {
                            return $api.getLeiloes();
                        }
                    }
                }
            }
        })
        .state('app.lotes', {
            url: '/lotes/:lotesID',
            views: {
                'menuContent': {
                    templateUrl: 'templates/lotes.html',
                    controller: 'lotesCtrl as vm',
                    resolve: {
                        requestLotes: function($api, $stateParams) {
                            // return $api.getLotes(33);
                            return $api.getLotes($stateParams.lotesID);
                        }
                    }
                }
            }
        });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/login');
        // Add the interceptor to the $httpProvider.
        $httpProvider.interceptors.push('httpInterceptor');
    });

})();